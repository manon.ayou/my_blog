<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Asset\Package;

class BlogController extends AbstractController
{
    #[Route('/blog', name: 'blog')]
    public function index(): Response
    {
        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
        ]); #Il s'agit d'une docstring, qui définie la route pour ce contrôleur
    }

    public function add()
    {
        return $this->render('blog/add.html.twig', [
            'controller_name' => 'BlogController',
        ]);
    }

    public function show($url)
    {
        return $this->render('blog/show.html.twig', [
            'controller_name' => 'BlogController',
            'slug' => $url,
        ]);
    }

    public function edit($id)
    {
        return $this->render('blog/edit.html.twig', [
            'controller_name' => 'BlogController',
            'slug' => $id,
        ]);
    }

    public function remove($id)
    {
        return new Response('<h1>Supprimer l\'article ' .$id. '</h1>');
    }
}
